Aws::CF::Signer.configure do |config|
  config.key_path = File.join(Rails.root,'config','certs','cloudfront.pem')
  config.key_pair_id = Alonetone.cloudfront_key_id
  config.default_expires = 31556920
end if Alonetone.cloudfront_enabled
